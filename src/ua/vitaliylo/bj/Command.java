package ua.vitaliylo.bj;

import com.google.gson.Gson;

/**
 * Created by vuzya on 15.04.14.
 */
public class Command {
    static Gson gson = new Gson();

    public static final int COMMAND_ERROR = 500;
    public static final int COMMAND_CONNECTED = 1;

    public Command(){
    }

    public Command(int command, String payload){
        this.command = command;
        this.payload = payload;
    }

    public int command;

    public String payload;

    @Override
    public String toString() {
        return gson.toJson(this);
    }
}
