package ua.vitaliylo.bj.dealer;

import ua.vitaliylo.bj.Command;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by vuzya on 13.04.14.
 */
public class Table {
    Map<Integer, Thread> clients = new ConcurrentHashMap<Integer, Thread>();

    public boolean start() {
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(12321);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(ss==null){
            return false;
        }

        System.out.println("Socket is set up!");

        while(true){
            try {
                Socket socket = ss.accept();
                if(clients.size()>=7){
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                    writer.write(new Command(Command.COMMAND_ERROR, "Table is full").toString());
                    writer.write("\n");
                    writer.flush();
                    System.out.println("Table is full!");
                    writer.close();
                    socket.close();
                }else{
                    int pos = 0;
                    synchronized (clients){
                        Thread thread = new Thread(new ClientHandler(socket));
                        while (clients.containsKey(pos)){
                            pos++;
                        }
                        clients.put(pos, thread);
                        thread.start();
                    }
                    System.out.println("Connected! Position is " + pos);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
