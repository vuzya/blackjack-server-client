package ua.vitaliylo.bj.dealer;

import ua.vitaliylo.bj.Command;

import java.io.*;
import java.net.Socket;

/**
 * Created by vuzya on 15.04.14.
 */
public class ClientHandler implements Runnable {
    private final Socket socket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private OnDisconnectListener listener;

    public ClientHandler(Socket socket){
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
            if (listener != null){
                listener.onDisconnected();
            }
            if(socket.isConnected()){
                try {
                    socket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        if(reader == null || writer == null){
            return;
        }

        try {
            writer.write(new Command(Command.COMMAND_CONNECTED, "Connected!").toString());
            writer.write("\n");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        synchronized (this){
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
    }

    public static interface OnDisconnectListener{
        public void onDisconnected();
    }
}
