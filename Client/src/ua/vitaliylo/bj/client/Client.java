package ua.vitaliylo.bj.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.Socket;

public class Client {

    public static void main(String[] args) {
	    String host = "localhost";
        int port = 12321;
        if(args.length>0){
            host = args[0];
        }
        if(args.length>1){
            port = Integer.parseInt(args[1]);
        }

        Socket socket = null;
        try {
            socket = new Socket(host, port);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(socket == null){
            return;
        }
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String line;
            while(!socket.isClosed() && (line = reader.readLine()) != null){
                System.out.println(line);
            }
            reader.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
